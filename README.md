# Basic Credit Card Processing
## by Daniel Ochoa <daniel.o.ochoa@gmail.com>

To setup:
 - Run `npm install` in project directory to install dependencies.
 - Written using node v9.x so you may want to have that environment setup beforehand.
 - To run tests, `npm run test`.

To run main script:
 - As per the instructions, `./index.js input.txt` or `./index.js < input.txt` should work. Just note that the
     filename or stdin file location on this example assumes the input.txt file is located in the same directory.
     (I included it in this project.)

### Reasoning

I chose javascript since it's my favorite language and also because it is the one I know the best so I tend to work
faster in it than in other languages.

I split the project into `index.js`, which is the main entry point to get the desired output. I split the
functionality into:
 - An `InputParser` class that handles all concerns for parsing input either from a file name or
STDIN.
 - A `Store` class to handle user records. This is kept in memory, but it could potentially be extended to connect
     ot a database or some other more persistent store.
 - A `validators` module that handles luhn 10 validation and a `logger` module that provides some logging
     functionality for formatting output.

