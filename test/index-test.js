const assert = require('assert');
const { exec } = require('child_process');

const expectedOutput = 'Lisa: $-93\nQuincy: error\nTom: $500\n';

describe('index.js', () => {
  describe('when using STDIN', () => {
    it('should properly print out the desired output', done => {
      exec('./index.js < input.txt', (error, stdout/*, stderr */) => {
        assert.equal(stdout, expectedOutput);
        done();
      });
    });
  });

  describe('when passing a file name', () => {
    it('should properly print out the desired output', done => {
      exec('./index.js input.txt', (error, stdout/*, stderr */) => {
        assert.equal(stdout, expectedOutput);
        done();
      });
    });
  });
});
