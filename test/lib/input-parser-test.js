const assert = require('assert');
const InputParser = require('../../lib/input-parser');
const fs = require('fs');

const expectedOutput = [
  [ 'Add', 'Tom', '4111111111111111', '$1000' ],
  [ 'Add', 'Lisa', '5454545454545454', '$3000' ],
  [ 'Add', 'Quincy', '1234567890123456', '$2000' ],
  [ 'Charge', 'Tom', '$500' ],
  [ 'Charge', 'Tom', '$800' ],
  [ 'Charge', 'Lisa', '$7' ],
  [ 'Credit', 'Lisa', '$100' ],
  [ 'Credit', 'Quincy', '$200' ]
];

describe('input-parser.js', () => {
  describe('when passing a file name', () => {
    it('should parse the file contents', done => {
      new InputParser({ fileName: 'input.txt' })
        .parse()
        .then(response => {
          assert.deepEqual(response, expectedOutput);
          done();
        }).catch(err => {
          assert.fail(err);
          done();
        });
    });
  });

  describe('when processing STDIN', () => {
    it('should parse the file contents', done => {
      new InputParser({ input: fs.createReadStream(`${process.cwd()}/input.txt`) })
        .parse()
        .then(response => {
          assert.deepEqual(response, expectedOutput);
          done();
        });
    });
  });
});
