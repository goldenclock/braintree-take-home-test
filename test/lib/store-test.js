const assert = require('assert');
const Store  = require('../../lib/store');

const entries = [
  [ 'Add', 'Tom', '4111111111111111', '$1000' ],
  [ 'Add', 'Lisa', '5454545454545454', '$3000' ],
  [ 'Add', 'Quincy', '1234567890123456', '$2000' ],
  [ 'Charge', 'Tom', '$500' ],
  [ 'Charge', 'Tom', '$800' ],
  [ 'Charge', 'Lisa', '$7' ],
  [ 'Credit', 'Lisa', '$100' ],
  [ 'Credit', 'Quincy', '$200' ]
];

const store = new Store();

describe('store.js', () => {
  describe('process', () => {

    beforeEach(() => {
      store.empty();
    });

    it('sound contain a records object with users data', () => {
      entries.forEach(entry => store.process(entry));
      assert.ok(store.records['Tom']);
      assert.ok(store.records['Quincy']);
      assert.ok(store.records['Lisa']);
    });

    it('should have ccNumber, limit, validCC and balance on each record', () => {
      entries.forEach(entry => store.process(entry));
      const tom = store.records['Tom'];
      assert.equal(tom.ccNumber,  '4111111111111111');
      assert.equal(tom.limit, 1000);
      assert.equal(tom.validCC, true);
      assert.equal(tom.balance, 500);
    });
  });

  describe('person operations', () => {
    before(() => {
      store.addPerson('Tom', '4111111111111111', 50000);
    });
    describe('addPerson', () => {
      it('should be able to add a person to our records collection', () => {
        assert.equal(store.records['Tom'].limit, 50000);
        assert.equal(store.records['Tom'].balance, 0);
        assert.equal(store.records['Tom'].validCC, true);
      });
    });
    describe('chargePerson', () => {
      it('should properly add a balance to a person within the limit', () => {
        store.chargePerson('Tom', 49000);
        assert.equal(store.records['Tom'].balance, 49000);
      });
      it('should not add the balance if it goes above the limit', () => {
        store.chargePerson('Tom', 2000);
        assert.equal(store.records['Tom'].balance, 49000);
      });
      it('should be able to credit a person', () => {
        store.creditPerson('Tom', 45000);
        assert.equal(store.records['Tom'].balance, 4000);
      });
      it('should let you credit a person into negative numbers', () => {
        store.creditPerson('Tom', 5000);
        assert.equal(store.records['Tom'].balance, -1000);
      });
    });
  });

  describe('_parseAmount', () => {
    it('should correctly parse amount numbers as strings with the `$` character', () => {
      assert.equal(store._parseAmount('$10000'), 10000);
      assert.equal(store._parseAmount('10000'), 10000);
      assert.equal(store._parseAmount('$50000'), 50000);
    });
  });
});
