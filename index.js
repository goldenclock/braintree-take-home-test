#!/usr/bin/env node

'use strict';

const InputParser = require('./lib/input-parser');
const Store       = require('./lib/store');
const logger      = require('./lib/logger');

const parser = new InputParser({
  fileName: process.argv[2],
  input: process.stdin,
});

const store = new Store();

parser.parse()
  .then(parsedInput => {
    parsedInput.forEach(lineItem => store.process(lineItem));
    logger.programOutputLogger(store.records);
  });
