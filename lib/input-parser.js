
const fs       = require('fs');
const readline = require('readline');

const cwd = process.cwd();

/**
 * Parses input either from argv or STDIN.
 * If we have a fileName, use it instead of input (STDIN).
 */
class InputParser {
  constructor({ fileName, input }) {
    this.transactions = [];
    this.fileName     = null;
    this.reader       = null;

    if (fileName) {
      this.fileName = fileName;
    } else {
      this.reader = this._createSTDINReader(input, process.stdout, false);
    }
  }

  /**
   * Main entry point of the input parser.
   *
   * @return {Promise} Returns a promise that will resolve with an array of results.
   */
  parse() {
    return new Promise(this._parse.bind(this));
  }

  _parse(resolve/*, reject */) {
    if (this.reader) {
      this.reader
        .on('line', this._processLine.bind(this))
        .on('close', () => resolve(this.transactions));
    } else {
      this._processFileContents(fs.readFileSync(`${cwd}/${this.fileName}`));
      resolve(this.transactions);
    }
  }

  _processLine(line) {
    this.transactions.push(line.split(' '));
  }

  _processFileContents(content) {
    const lines = this._normalize(content);
    this.transactions = this.transactions.concat(lines);
  }

  _createSTDINReader(input, output, terminal) {
    return readline.createInterface({ input, output, terminal });
  }

  _normalize(content) {
    return content.toString()
      .split('\n')
      .filter(item => item.length)
      .map(item => item.split(' '));
  }
}

module.exports = InputParser;
