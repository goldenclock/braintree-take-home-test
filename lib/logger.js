
const chalk = require('chalk');

const PREFIX        = 'BrainTree';
const SEPARATOR     = ': ';
const DEFAULT_COLOR = 'green';

module.exports = {
  stringify(message) {
    return chalk[DEFAULT_COLOR](`${PREFIX}${SEPARATOR}`) + String(message);
  },

  log(message) {
    console.log(this.stringify(message));
  },

  /**
   * This function will log into STDOUT in the format required for this exercise.
   *
   * @param {object} The Object is the one specified by the store class `records` object.
   */
  programOutputLogger(records) {
    const names = Object.keys(records).sort();
    names.forEach(key => {
      const record = records[key];
      const balance = record.validCC ? `$${record.balance}` : 'error';
      console.log(`${key}: ${balance}`);
    });
  }
};
