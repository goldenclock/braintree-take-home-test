
const logger     = require('./logger');
const validators = require('./validators');

/**
 * Acts as the store of our transactions. You should be able to run some methods on the store to get data for any
 * particular user.
 */
class Store {
  constructor() {
    this.records = {};
  }

  /**
   * Main entry point for the store. The argument should be an array that contains an action, name, credit card
   * number (only for adding) and amount as string.
   */
  process(lineItem) {
    const actionType = lineItem[0].toUpperCase().trim();
    switch (actionType) {
    case 'ADD': {
      this.addPerson(lineItem[1], lineItem[2], this._parseAmount(lineItem[3]));
      break;
    }
    case 'CHARGE': {
      this.chargePerson(lineItem[1], this._parseAmount(lineItem[2]));
      break;
    }
    case 'CREDIT':
      this.creditPerson(lineItem[1], this._parseAmount(lineItem[2]));
      break;
    default:
      throw new Error(logger.stringify(`Unrecognized action: ${actionType} encountered.`));
    }
  }

  addPerson(person, ccNumber, amount) {
    const isValid = validators.validateCC(ccNumber);
    this.records[person] = this._newRecord(ccNumber, amount, isValid);
  }

  chargePerson(person, amount) {
    const record = this.records[person];
    if (record && record.validCC) {
      const newBalance = record.balance + amount;
      if (newBalance > record.limit) return;
      record.balance = newBalance;
    }
  }

  creditPerson(person, amount) {
    const record = this.records[person];
    if (record && record.validCC) {
      record.balance = record.balance - amount;
    }
  }

  _newRecord(ccNumber, limit, validCC) {
    return { ccNumber, limit, validCC, balance: 0 };
  }

  _parseAmount(stringAmount) {
    return Number(stringAmount.replace(/[$]/, ''));
  }

  empty() {
    this.records = {};
    return true;
  }
}

module.exports = Store;
