
const luhn = require('luhn');

module.exports = {

  /**
   * Validates Credit Card numbers using the luhn 10 algorithm.
   *
   * @return {string}
   */
  validateCC(number) {
    return luhn.validate(number);
  }
};
